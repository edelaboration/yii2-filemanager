<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model robote13\filemanager\models\File */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    '$("document").ready(function(){
        $("#new-record").on("pjax:end", function() {
            $.pjax.reload({container:"#view-index"});  //Reload GridView
        });
    });'
);
?>

<div class="file-form">
    <?php Pjax::begin(['id'=>'new-record']); ?>
        <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
            <?=$form->errorSummary($model)?>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'attr_title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'attr_alt')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'file')->widget('\trntv\filekit\widget\Upload',[
                            'url' => ['files/upload'],
                            'maxFileSize' => $this->context->module->maxFileSize,
                            'clientOptions' => [
                                'done'=>new yii\web\JsExpression('function(e,data){$(".upload-kit-item").each(function(index, item){
                                    $(item).find("input[data-role=order]").val(index);})}'
                                )
                            ]
                        ]
                    ); ?>
                    <?php if($this->context->module->isUsedWatermarks()):?>
                        <?= $form->field($model,'watermark')->checkbox()?>
                    <?php endif;?>
                </div>

                <div class="form-group col-sm-2">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('robote13/filemanager', 'Upload') : Yii::t('robote13/filemanager', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-lg pull-right' : 'btn btn-primary']) ?>
                </div>
            </div>

        <?php ActiveForm::end();?>
    <?php Pjax::end();?>
</div>
