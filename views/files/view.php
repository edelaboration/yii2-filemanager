<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model robote13\filemanager\models\File */

$this->title = $model->filename;
$this->params['breadcrumbs'][] = ['label' => Yii::t('robote13/filemanager', 'Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-view">
    <p>
        <?= Html::a(Yii::t('robote13/filemanager', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('robote13/filemanager', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('robote13/filemanager', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'filename',
            'urlAttribute',
            [
                'label'=> Yii::t('robote13/filemanager','Link'),
                'value'=>Html::a('Link',$model->urlAttribute,['target'=>'_blank']),
                'format'=>'raw'
            ],
            'type',
            'size',
            'order',
            'attr_alt',
            'attr_title',
            'description:ntext',
            'uploaded_at',
        ],
    ]) ?>

</div>
