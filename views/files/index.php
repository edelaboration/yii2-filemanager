<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel robote13\filemanager\models\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('robote13/filemanager', 'Files');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-index">
    <div class="panel panel-default">
        <div class="panel-heading"><?=Yii::t('robote13/filemanager', 'Upload a file')?></div>
        <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
        </div>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(['id'=>'view-index']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'uploaded_at',
                'filename',
                'type',
                'attr_alt',
                'attr_title',
                'urlAttribute',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
