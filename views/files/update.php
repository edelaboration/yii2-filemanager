<?php

/* @var $this yii\web\View */
/* @var $model robote13\filemanager\models\File */

$this->title = Yii::t('robote13/filemanager', 'Update file: {title}', ['title' => $model->filename]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('robote13/filemanager', 'Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
