<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model robote13\filemanager\models\File */

$this->title = Yii::t('robote13/filemanager', 'Create File');
$this->params['breadcrumbs'][] = ['label' => Yii::t('robote13/filemanager', 'Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
