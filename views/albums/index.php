<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel robote13\filemanager\models\AlbumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

omnilight\assets\FancyBoxAsset::register($this);

$this->title = Yii::t('robote13/filemanager', 'Albums');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs("$('.fBox').fancybox();",  \yii\web\View::POS_READY);
?>
<div class="album-index">
    <div class="panel panel-default">
        <div class="panel-heading"><?=Yii::t('robote13/filemanager', 'Add a new album')?></div>
        <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
        </div>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(['id'=>'view-index','timeout'=>5000]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'export' => false,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function ($model, $key, $index) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detailUrl' => 'view'
                ],
                'title',
                'description:ntext',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
