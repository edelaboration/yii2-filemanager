<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model robote13\filemanager\models\Album */
/* @var $form yii\widgets\ActiveForm */
/* @var $module robote13\filemanager\Module */

$module = $this->context->module;

$this->registerJs(
    '$("document").ready(function(){
        $("#new-record").on("pjax:end", function() {
            $.pjax.reload("#view-index",{timeout:10000});  //Reload GridView
        });
    });'
);
?>

<div class="album-form">
    <?php Pjax::begin(['id'=>'new-record','enablePushState'=>false,'timeout'=>5000]); ?>
        <?php $form = ActiveForm::begin([
            'options' => ['data-pjax' => true,'class'=>'row'],
        ]);?>

            <div class="col-sm-5">
                <?=$form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                <?=$form->field($model, 'description')->textarea(['rows' => 2]) ?>
            </div>

            <div class="col-sm-7">

                <?=$form->field($model, 'attached_files')->widget('\trntv\filekit\widget\Upload',[
                        'url' => ['files/upload'],
                        'sortable' => true,
                        'maxFileSize' => $module->maxFileSize,
                        'maxNumberOfFiles' => $module->maxFilesInAlbum,
                        'clientOptions' => [
                            'done'=>new yii\web\JsExpression('function(e,data){$(".upload-kit-item").each(function(index, item){
                                $(item).find("input[data-role=order]").val(index);})}'
                            )
                        ]
                    ]
                );?>

                <!--?php if($this->context->module->isUsedWatermarks()):?>
                    <= $form->field($model,'watermark')->checkbox()?>
                <php endif;?-->
            </div>

            <div class="form-group col-sm-12">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('robote13/filemanager', 'Create') : Yii::t('robote13/filemanager', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-lg btn-success pull-right' : 'btn btn-lg btn-primary pull-right']) ?>
            </div>
        <?php ActiveForm::end();?>
    <?php Pjax::end();?>
</div>
