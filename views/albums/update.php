<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model robote13\filemanager\models\Album */

$this->title = Yii::t('robote13/filemanager', 'Update album: {title}', ['title' => $model->title]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('robote13/filemanager', 'Albums'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="album-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
