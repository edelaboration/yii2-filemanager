<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model robote13\filemanager\models\Album */
omnilight\assets\FancyBoxAsset::register($this);
?>

<?=ListView::widget([
    'layout'=>"{items}",
    'emptyText' => Html::tag('p',Yii::t('robote13/filemanager','EmptyAlbum')),
    'dataProvider'=>new yii\data\ArrayDataProvider(['allModels'=>$model->files]),
    'options'=>['class'=>'list-view-files clearfix'],
    'itemView'=>function($file)use($model){
        $arr = explode('/', $file->path);
        return Html::a(Html::img($file->base_url.'/'. $arr[0].'/small/' . $arr[1]),"{$file->base_url}/{$file->path}",['title'=>$file->description,'data-pjax'=>0,'class'=>'fBox','rel'=>$model->id]);
    },
    'itemOptions'=>['class'=>'pull-left']
]);