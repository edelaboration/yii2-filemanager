<?php

use yii\db\Migration;

/**
 * Handles the creation and droping for table `module_tables` in the database.
 */
class m160327_154405_create_module_tables extends Migration
{
    public $albumTable = "{{%album}}";
    public $fileTable = "{{%file}}";
    public $relTable = "{{%albums_files}}";
    protected $tableOptions;
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->albumTable, [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(null),
            'created_at' => $this->timestamp()->notNull()->defaultExpression("CURRENT_TIMESTAMP"),
            'updated_at' => $this->timestamp()->defaultValue(null)
        ], $this->tableOptions);
        
        $this->createTable($this->fileTable,[
            'id' => $this->primaryKey(),
            'path' => $this->string()->notNull(),
            'filename' => $this->string()->notNull(),
            'base_url' => $this->string()->notNull(),
            'type' => $this->string(128)->notNull(),
            'size' => $this->integer()->notNull(),
            'order' => $this->integer()->notNull(),
            'attr_alt' => $this->string()->defaultValue(null),
            'attr_title' => $this->string()->defaultValue(null),
            'description' => $this->text()->defaultValue(null),
            'uploaded_at' => $this->timestamp()->notNull()->defaultExpression("CURRENT_TIMESTAMP")
        ], $this->tableOptions);
        
        $this->createTable($this->relTable,[
            'album_id' =>  $this->integer()->notNull(),
            'file_id' =>  $this->integer()->notNull()
        ], $this->tableOptions);
        
        $this->addPrimaryKey("", $this->relTable, ['album_id','file_id']);
        $this->createIndex('fk_albums_files_album_idx', $this->relTable, 'album_id');
        $this->createIndex('fk_albums_files_file_idx', $this->relTable, 'file_id');
        
        $this->addForeignKey('fk_albums_files_album', $this->relTable,'album_id', $this->albumTable, 'id', "CASCADE", "CASCADE");
        $this->addForeignKey('fk_albums_files_file', $this->relTable,'file_id', $this->fileTable, 'id', "CASCADE", "CASCADE");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->relTable);
        $this->dropTable($this->albumTable);
        $this->dropTable($this->fileTable);
    }
    
    public function init() {
        parent::init();
        
        $this->tableOptions = \Yii::$app->db->driverName == 'mysql'?'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci':null;
    }
}
