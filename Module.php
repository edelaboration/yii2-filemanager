<?php

namespace robote13\filemanager;

use Yii;
use robote13\filemanager\processors;

/**
 * Filemanager Module.
 *
 * @property string|false $watermark path to file contained watermark image
 */
class Module extends \yii\base\Module
{
    public $defaultRoute = 'main';

    public $controllerNamespace = 'robote13\filemanager\controllers';

    /**
     * Maximum number of files in the album.
     * @var integer
     */
    public $maxFilesInAlbum = 99;

    /**
     * Maximal file size in bytes.
     * @var integer
     */
    public $maxFileSize = 10485760; //10Mb

    private $_defaultProcessors = [
        'thumbs' => [
            'class' => processors\ThumbsMaker::class,
            'thumbs' => ['small' => [100,100]]
        ],
        'watermark' => [
            'class' => processors\WatermarkOverlay::class,
        ],
    ];

    public function __construct($id, $parent = null, $config = array()) {
        $this->attachBehaviors($this->_defaultProcessors);
        parent::__construct($id, $parent, $config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if(!Yii::$app->hasModule('gridview'))
        {
            Yii::$app->setModule('gridview',[
                'class'=> \kartik\grid\Module::className()
            ]);
        }
    }

    /**
     * Wrapper.
     * Default processors will be detached
     * @param processors\PostActionProcessor $processors
     */
    public function setProcessors($processors)
    {
        $this->attachBehaviors($processors);
    }

    /**
    *
    */
    public static function t($message, $params = [], $language = null)
    {
        return Yii::t('robote13/filemanager', $message, $params, $language);
    }
}
