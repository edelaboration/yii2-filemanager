<?php

namespace robote13\filemanager;

use Yii;

/**
 * Description of Bootstrap
 *
 * @author Tartharia
 */
class Bootstrap implements \yii\base\BootstrapInterface
{
     /**
     *
     * @param \yii\base\Application $app
     */
    public function bootstrap($app) {
        Yii::$container->set(\trntv\filekit\Storage::className(), Storage::className());
        if($app instanceof \yii\web\Application)
        {
            Yii::$app->i18n->translations['robote13/filemanager'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en-US',
                'basePath' => __DIR__ . '/messages',
                'fileMap'=>[
                    'robote13/filemanager'=>'filemanager.php'
                ]
            ];
        }
    }
}
