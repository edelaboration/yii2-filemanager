<?php

namespace robote13\filemanager\processors;

use yii\base\Event;
use trntv\filekit\actions\UploadAction;
use trntv\filekit\actions\DeleteAction;

/**
 * Description of PostActionProcessor
 *
 * @author Tartharia
 */
abstract class PostActionProcessor extends \yii\base\Behavior
{

    /**
     * @return []
     */
    abstract public function postActionEvents();

    /**
     * Attaches the processor object to the action.
     * Attach event handlers as declared in [[postActionEvents]].
     */
    public function attach($owner)
    {
        parent::attach($owner);
        foreach ($this->postActionEvents() as $eventName => $handler)
        {
            Event::on($this->resolveClass($eventName), $eventName,[$this,$handler]);
        }
    }

    public function detach()
    {
        parent::detach();
        foreach ($this->postActionEvents() as $eventName => $handler)
        {
            Event::off($this->resolveClass($eventName), $eventName, $handler);
        }
    }

    private function resolveClass($eventName)
    {
        switch ($eventName) {
            case UploadAction::EVENT_AFTER_SAVE:
                return UploadAction::class;
            case DeleteAction::EVENT_AFTER_DELETE:
                return DeleteAction::class;
            default:
                throw new \LogicException('Only events from `trntv\filekit\actions` allowed.');
        }
    }
}
