<?php

namespace robote13\filemanager\processors;

use Yii;

/**
 * Description of WatermarkOverlay
 *
 * @property string $watermark Path to file with watermark image
 * @author Tartharia
 */
class WatermarkOverlay extends PostActionProcessor
{
    /**
     * Path to file with watermark image.
     * @var string
     */
    private $_watermark;

    /**
     * @inheritdoc
     */
    public function postActionEvents(){
        return [\trntv\filekit\actions\UploadAction::EVENT_AFTER_SAVE => 'afterSave'];
    }

    /**
     * Setter for watermark property.
     * @see WatermarkOverlay::$watermark
     * @param string $filename
     */
    public function setWatermark($filename)
    {
        $this->_watermark = Yii::getAlias($filename);
    }

    /**
     * Getter for watermark property.
     * @see WatermarkOverlay::$watermark
     * @return string Path to the file
     */
    public function getWatermark()
    {
        return $this->_watermark;
    }

    /**
     *
     * @param \trntv\filekit\events\UploadEvent $event
     */
    public function afterSave($event)
    {
        if(!$this->isUsedWatermarks())
        {
            return;
        }

        $file = $event->file;
        $basePath = Yii::$app->fileStorage->getFileSystem()->getAdapter()->getPathPrefix();
        //$arr = explode('/', $file->getPath());
        $protect = new \Imagick($basePath . $file->getPath());
        $watermark = new \Imagick($this->watermark);
        $protect->compositeimage($watermark, \Imagick::COMPOSITE_OVER, 0, 0);
        $protect->writeimage($basePath . $file->getPath());
        $protect->destroy();
        $watermark->destroy();
    }

    /**
     * Показывает установлен ли путь к файлу с водяными знаками.
     * @return bool
     */
    public function isUsedWatermarks():bool {return isset($this->watermark);}
}
