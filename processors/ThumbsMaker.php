<?php

namespace robote13\filemanager\processors;

use trntv\filekit\actions;

use Yii;
use yii\helpers\FileHelper;
use trntv\filekit\events\UploadEvent;

/**
 * Description of ThumbsMaker
 *
 * @author Tartharia
 */
class ThumbsMaker extends PostActionProcessor
{
    public $thumbs = ['small'=>[100,100]];

    public function postActionEvents() {
        return[
            actions\UploadAction::EVENT_AFTER_SAVE => 'afterUpload',
            actions\DeleteAction::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    /**
     *
     * @param UploadEvent $event
     */
    public function afterUpload($event)
    {
        $file = $event->file;
        $arr = explode('/', $file->getPath());
        $basePath = Yii::$app->fileStorage->getFileSystem()->getAdapter()->getPathPrefix();

        foreach ($this->thumbs as $thumbVariant => $thumbSize)
        {
            try {
                $thumb = new \Imagick($basePath . $file->getPath());
            } catch (\Throwable $exc) {
                return;
            }

            if(!$thumb->cropthumbnailimage($thumbSize[0],$thumbSize[1]))
            {
                break;
            }
            $thumbsPath = $basePath . $arr[0] . DIRECTORY_SEPARATOR . $thumbVariant;
            FileHelper::createDirectory($thumbsPath);
            $thumb->writeimage($thumbsPath . DIRECTORY_SEPARATOR . $arr[1]);
            $thumb->destroy();
        }
    }

    /**
     *
     * @param UploadEvent $event
     */
    public function afterDelete($event)
    {
        $file = $event->file;
        $arr = explode('/', $file->getPath());
        foreach ($this->thumbs as $thumbVariant => $thumbSize)
        {
            Yii::$app->fileStorage->delete($arr[0].DIRECTORY_SEPARATOR.$thumbVariant.DIRECTORY_SEPARATOR.$arr[1]);
        }
    }
}
