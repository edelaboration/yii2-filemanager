<?php

namespace robote13\filemanager\controllers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use robote13\filemanager\models\Album;
use robote13\filemanager\models\AlbumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AlbumsController implements the CRUD actions for Album model.
 */
class AlbumsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Album models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember('','albums_index');
        $model = new Album();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model = new Album();
        }
        
        $searchModel = new AlbumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider->query->joinWith('files');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * Displays a single Album model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id = null)
    {
        if(Yii::$app->getRequest()->isAjax)
        {
            try {
                $model = $this->findModel(Yii::$app->getRequest()->post('expandRowKey'));
            } catch (NotFoundHttpException $exc) {
                return Html::tag('div', Yii::t('robote13/filemanager', 'AlbumNotFound'), ['class'=>'alert alert-info']);
            }
            return $this->renderPartial('_files',['model'=>$model]);
        }else{
            return $this->render('view',['model'=>  $this->findModel($id)]);
        }
    }

    /**
     * Updates an existing Album model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $url = Url::previous('albums_index');
            return $this->redirect(isset($url)?$url:['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Album model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Album model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Album the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Album::find()->joinWith('files')->andWhere([Album::tableName().'.id'=>$id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
