<?php

/*
 * The MIT License
 *
 * Copyright 2016 Tartharia.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace robote13\filemanager;

use Yii;
use League\Flysystem\MountManager;

/**
 * Description of MountBuilder
 *
 * @author Tartharia
 */
class MountBuilder{
    /**
     * Build filesystem manager.
     * @param array $adapters конфигурационный массив вида:
     * ```php
     * [
     *  'prefix'=>[
     *      'class'=>League\Flysystem\AdapterInterface::className(),
     *      'root'=>'path/to/root_dir'],
     *  ],
     *  ...
     * ]
     * ```
     * @return MountManager
     */
    public static function build($adapters)
    {
        return function() use ($adapters){
            $manager = new MountManager();
            foreach ($adapters as $prefix => $adapterConfig)
            {
                $adapter = new $adapterConfig['class'](Yii::getAlias($adapterConfig['root']));
                $manager->mountFilesystem($prefix, new \League\Flysystem\Filesystem($adapter));
            }
            return $manager;
        };
    }
}
