<?php

namespace robote13\filemanager\models;

use Yii;
use yii\db\BaseActiveRecord;

/**
 * This is the model class for table "{{%album}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AlbumsFiles[] $albumsFiles
 * @property File[] $files
 */
class Album extends \yii\db\ActiveRecord
{
    public $attached_files = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%album}}';
    }
    
    public function behaviors() {
        return [
            'timestamp'=>[
                'class' => '\yii\behaviors\AttributeBehavior',
                'attributes'=>[
                    BaseActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
                'value' => function(){return date("Y-m-d H:i:s");}
            ],
            'attachedFiles'=>[
                'class' => 'trntv\filekit\behaviors\UploadBehavior',
                'multiple' => true,
                'attribute' => 'attached_files',
                'uploadRelation' => 'files',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
                'typeAttribute' => 'type',
                'sizeAttribute' => 'size',
                'nameAttribute' => 'filename',
                'orderAttribute' => 'order'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            ['attached_files','safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('robote13/filemanager', 'ID'),
            'title' => Yii::t('robote13/filemanager', 'Title'),
            'description' => Yii::t('robote13/filemanager', 'Description'),
            'created_at' => Yii::t('robote13/filemanager', 'Created At'),
            'updated_at' => Yii::t('robote13/filemanager', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbumsFiles()
    {
        return $this->hasMany(AlbumsFiles::className(), ['album_id' => 'id'])->inverseOf('album');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['id' => 'file_id'])->via('albumsFiles')->orderBy(File::tableName().'.order');
    }

    /**
     * @inheritdoc
     * @return AlbumQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AlbumQuery(get_called_class());
    }
}
