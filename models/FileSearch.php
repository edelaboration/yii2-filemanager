<?php

namespace robote13\filemanager\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use robote13\filemanager\models\File;

/**
 * FileSearch represents the model behind the search form about `robote13\filemanager\models\File`.
 */
class FileSearch extends File
{
    public $ids;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'size', 'order'], 'integer'],
            ['ids','each','rule'=>['integer']],
            [['path', 'filename', 'base_url', 'type', 'attr_alt', 'attr_title', 'description', 'uploaded_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = File::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $sort = $dataProvider->getSort();

        $this->load($params);

        //if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
        //    return $dataProvider;
        //}

        $sort->defaultOrder=[
            'id'=>SORT_DESC
        ];

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'size' => $this->size,
            'order' => $this->order,
            'uploaded_at' => $this->uploaded_at,
        ]);
        $query->andFilterWhere(['in','id',  $this->ids])
            ->andFilterWhere(['like', 'path', $this->path])
            ->andFilterWhere(['like', 'filename', $this->filename])
            ->andFilterWhere(['like', 'base_url', $this->base_url])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'attr_alt', $this->attr_alt])
            ->andFilterWhere(['like', 'attr_title', $this->attr_title])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
