<?php

namespace robote13\filemanager\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "{{%file}}".
 *
 * @property integer $id
 * @property string $path
 * @property string $filename
 * @property string $base_url
 * @property string $type
 * @property integer $size
 * @property integer $order
 * @property string $attr_alt
 * @property string $attr_title
 * @property string $description
 * @property string $uploaded_at
 *
 * @property AlbumsFiles[] $albumsFiles
 * @property Album[] $albums
 */
class File extends \yii\db\ActiveRecord
{

    public $file;

    public $watermark = false;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%file}}';
    }

     public function behaviors()
     {
         return [
            'file' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
                'typeAttribute' => 'type',
                'sizeAttribute' => 'size',
                'nameAttribute' => 'filename',
              ],
          ];
     }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['attr_alt', 'attr_title'], 'string', 'max' => 255],
            [['watermark'],'boolean'],
            ['file','safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('robote13/filemanager', 'ID'),
            'path' => Yii::t('robote13/filemanager', 'Path'),
            'file' => Yii::t('robote13/filemanager', 'File'),
            'urlAttribute' => Yii::t('robote13/filemanager', 'Path to file'),
            'filename' => Yii::t('robote13/filemanager', 'Filename'),
            'base_url' => Yii::t('robote13/filemanager', 'Base Url'),
            'type' => Yii::t('robote13/filemanager', 'Type'),
            'size' => Yii::t('robote13/filemanager', 'Size'),
            'order' => Yii::t('robote13/filemanager', 'Order'),
            'attr_alt' => Yii::t('robote13/filemanager', 'Attr Alt'),
            'attr_title' => Yii::t('robote13/filemanager', 'Attr Title'),
            'description' => Yii::t('robote13/filemanager', 'Description'),
            'uploaded_at' => Yii::t('robote13/filemanager', 'Uploaded At'),
            'watermark' => Yii::t('robote13/filemanager', 'Add a watermark'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbumsFiles()
    {
        return $this->hasMany(AlbumsFiles::className(), ['file_id' => 'id'])->inverseOf('file');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbums()
    {
        return $this->hasMany(Album::className(), ['id' => 'album_id'])->viaTable('{{%albums_files}}', ['file_id' => 'id']);
    }

    /**
     *
     * @param string $size preview folder name
     * @return string
     */
    public function getUrlAttribute($size = null)
    {
        $path = $this->path;
        if($size!==null)
        {
            $path = preg_replace('/(\/)/',"/{$size}/", $path);
        }
        return "{$this->base_url}/{$path}";
    }

    /**
     * @inheritdoc
     * @return FileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FileQuery(get_called_class());
    }
}
