<?php
return [
    'Add a new album' => 'Добавить альбом',
    'Albums' => 'Альбомы',
    'AlbumNotFound' => "Альбом не найден. Возможно он был удален.",
    'Attr Alt' => 'Атрибут alt',
    'Attr Title' => 'Атрибут title',
    'Create' => 'Создать',
    'Delete' => 'Удалить',
    'Description' => 'Описание',
    'EmptyAlbum' => 'В альбом не добавлено ни одного файла.',
    'File' => 'Файл',
    'Files' => 'Файлы',
    'Link' => 'Ссылка на файл',
    'Path to file' => 'Путь к файлу',
    'Title' => 'Название',
    'Type' => 'Тип файла',
    'Update' => 'Обновить',
    'Update album: {title}' => 'Обновление альбома: {title}',
    'Upload a file' => 'Загрузить файл',
    'Upload' => 'Загрузить',
    'Uploads' => 'Загрузки',
];

