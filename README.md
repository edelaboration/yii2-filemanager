#A file manager for Yii2.

Loading and storing files. Organize files into albums.

##Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run `php composer.phar require --prefer-dist robote13/yii2-filemanager "*"` or add 
`"robote13/yii2-filemanager": "*"` to the require section of your `composer.json` file.

##Usage

###Настройка хранилища:

Модуль ничего не знает о том где и как сохраняются файлы. 
Все операции по работе с хранилищем производятся при помощи абстрактной файловой системы [Flysystem](http://flysystem.thephpleague.com/).

Для работы с ней нужно сконфигурировать [Mount Manager](http://flysystem.thephpleague.com/mount-manager/) и компонент `fileStorage`. 
В модуль входит класс `robote13\filemanager\MountBuilder` с помощью которого вы сможете создать компонент управляющий хранилищами.

Пример конфигурации компонента с подключением хранилища для локальной файловой системы:

```
'components' => [
    'mountManager' => robote13\filemanager\MountBuilder::build([
        'local'=>[
            'class' => '\League\Flysystem\Adapter\Local',
            'root' => '@app/web/files'
        ]
    ]),
    'fileStorage' => [
        'class' => 'trntv\filekit\Storage',
        'baseUrl' => '@web/files',
        'maxDirFiles' => 1024, //для использования шардирования
        'filesystem' => function (){
            return Yii::$app->mountManager->getFileSystem('local'); // подключение к модулю хранилища
        }
    ],
    ...
]
```

или без использования MountManager

```
'fileStorage' => [
    'class' => 'trntv\filekit\Storage',
    'baseUrl' => '@web/files',
    'maxDirFiles' => 1024, //для использования шардирования
    'filesystem' => function(){
        $adapter = new League\Flysystem\Adapter\Local(Yii::getAlias('@app/web/files'));
        return new \League\Flysystem\Filesystem($adapter);
    }
],
```

###Подключение модуля

```
'modules' => [
    'filemanager' => [
        'class' => 'robote13\filemanager\Module',
        'watermark' => '@app/assets/watermark.png', //по умолчанию false
        'thumbs'=>[
            'small'=>[220,132],
            'frontend-preview'=>[60,80]
        ]
    ],
    ...
]
```
